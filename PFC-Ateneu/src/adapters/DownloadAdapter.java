package adapters;

import java.util.List;

import pfc.ateneu.objects.SharedFile;
/**
 * Adapter to upload the list of files in the user interface
 * 
 * Adaptador per c�rregar la llista de fitxers a la interfa� gr�fica
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 * 
 *
 */
public class DownloadAdapter {
	List<SharedFile> filesDownloadPanel = null; 
	/**
	 * @return Returns the list of files
	 * @return Retorna la llista de fitxers al panell
	 */
	public synchronized List<SharedFile> getFilesDownloadPanel() {
		return filesDownloadPanel;
	}
	/**
	 * Updates the list of the files in the UI
	 * Actualitza la llista dels fitxers a la UI
	 * @param filesDownloadPanel
	 */
	public synchronized void setFilesDownloadPanel(List<SharedFile> filesDownloadPanel) {
		this.filesDownloadPanel = filesDownloadPanel;
	}
}
