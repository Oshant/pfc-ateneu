package adapters;

import pfc.ateneu.objects.SearchSharedFiles;

/**
 * Stores the list of found files in server and the ones we want to download
 * 
 * Classe que permet enmagatzemar la llista dels fitxers trobats i els que es
 * volen descÓrregar
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class SearchAdapter {
	private SearchSharedFiles searchSharedFiles, filesAvailableToDownload;

	public synchronized SearchSharedFiles getSearchSharedFiles() {
		return searchSharedFiles;
	}

	public synchronized void setSearchSharedFiles(
			SearchSharedFiles searchSharedFiles) {
		this.searchSharedFiles = searchSharedFiles;
	}

	public synchronized void setFilesAvailableToDownload(
			SearchSharedFiles filesAvailableToDownload) {
		this.filesAvailableToDownload = filesAvailableToDownload;
		System.out.println("FILE AVAILABLE!!!!!!");
	}

	public synchronized SearchSharedFiles getFilesAvailableToDownload() {
		return filesAvailableToDownload;
	}

}
