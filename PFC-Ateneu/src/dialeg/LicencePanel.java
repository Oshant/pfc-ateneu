package dialeg;


import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
/**
 * Program licence
 * 
 * Llic�ncia del programa
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class LicencePanel extends JPanel {

	public LicencePanel() {
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setToolTipText("");
		scrollPane.setBounds(5, 5, 430, 123);
		add(scrollPane);
		
		JTextArea txtLicence = new JTextArea();
		txtLicence.setText("Licence:\n\nGPL � Ramon Padilla Mensa and Roger Bosch Gasc� 2015.\nYou can distribute and modify this software. \n\n\n\nEnjoy the software.");
		txtLicence.setToolTipText("");
		scrollPane.setViewportView(txtLicence);

	}

}
