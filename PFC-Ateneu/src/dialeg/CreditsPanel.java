package dialeg;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextArea;
import java.awt.Dimension;

public class CreditsPanel extends JPanel {

		
	/**
	 * Panel with information about the developers
	 * 
	 * Panell amb la informaci� dels desenvolupadors
	 * 
	 * @author Roger Bosch
 	 * @author Ramon Padilla
	 */
	public CreditsPanel() {
		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setToolTipText("");
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(5, 5, 430, 123);
		add(scrollPane);

		JTextArea txtrCreatedBy = new JTextArea();
		txtrCreatedBy.setPreferredSize(new Dimension(430, 160));
		txtrCreatedBy
				.setText("Created by Ramon Padilla Mensa and Roger Bosch Gasc�.\n\nYear 2015.\n\nInstitut Ausias March.\n\n\n\n\nThis software is open source and completely free.\n");
		scrollPane.setViewportView(txtrCreatedBy);

	}
}
