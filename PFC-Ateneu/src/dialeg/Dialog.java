package dialeg;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AbstractDocument.Content;

import java.awt.CardLayout;

import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.GridLayout;

import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JToggleButton;
import java.awt.Dimension;

/**
 * Information about Ateneu
 * 
 * Informaci� referent a Ateneu
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class Dialog extends JDialog {
	private JButton btLicence;
	private JButton btCredits;
	private JButton btClose;
	private JPanel iconPanel;

	private JLabel lblImage;
	private JPanel presentationPanel;
	private JPanel creditsPanel;
	private JLabel lblVersion;
	private JLabel lblExplanation;
	private JLabel lblCopyright;
	private JPanel centralPanel;
	private String show1 = "name_10397445893263",
			show2 = "name_10397445893263";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Dialog dialog = new Dialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Dialog() {
		setTitle("About Ateneu");
		setBounds(100, 100, 460, 315);
		// setContentPane(new JLabel(new ImageIcon("images/ateneu_logo.png")));
		ImageIcon img = new ImageIcon("images/ateneu_logo_100px.jpg");
		setIconImage(img.getImage());
		setResizable(false);
		getContentPane().setLayout(null);
		JPanel contentPanel = new JPanel();
		contentPanel.setBounds(0, 0, 440, 280);
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		centralPanel = new JPanel();
		centralPanel.setBounds(5, 100, 440, 127);
		contentPanel.add(centralPanel);
		centralPanel.setLayout(new CardLayout(0, 0));
		presentationPanel = new JPanel();
		centralPanel.add(presentationPanel, "name_10397445893263");
		presentationPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		presentationPanel.setLayout(null);
		{
			lblVersion = new JLabel("Version 1.0");
			lblVersion.setBounds(182, 12, 70, 15);
			presentationPanel.add(lblVersion);
		}
		{
			lblExplanation = new JLabel("Ateneu software");
			lblExplanation.setBounds(164, 30, 306, 15);
			presentationPanel.add(lblExplanation);
		}
		{
			lblCopyright = new JLabel(
					" �2015 Ramon Padilla Mensa and Roger Bosch Gasc�");
			lblCopyright.setBounds(136, 134, 178, 15);
			presentationPanel.add(lblCopyright);
		}

		creditsPanel = new CreditsPanel();
		creditsPanel.setPreferredSize(new Dimension(430, 120));
		centralPanel.add(creditsPanel, "name_10397462931204");

		LicencePanel panelllLlicencia = new LicencePanel();
		centralPanel.add(panelllLlicencia, "name_11676399906460");
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(3, 250, 440, 30);
			contentPanel.add(buttonPane);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[] { 80, 0, 85, 95, 76, 30,
					30, 0 };
			gbl_buttonPane.rowHeights = new int[] { 25, 0 };
			gbl_buttonPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
					0.0, 0.0, 0.0, Double.MIN_VALUE };
			gbl_buttonPane.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
			buttonPane.setLayout(gbl_buttonPane);
			{
				btLicence = new JButton("Licence");
				btLicence.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (show1.equals("name_10397445893263"))
							show1 = "name_11676399906460";
						else
							show1 = "name_10397445893263";
						show2 = "name_10397445893263";
						CardLayout cl = (CardLayout) (centralPanel.getLayout());
						cl.show(centralPanel, show1);
					}
				});
			}
			{
				btCredits = new JButton("Credits");
				btCredits.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (show2.equals("name_10397445893263"))
							show2 = "name_10397462931204";
						else
							show2 = "name_10397445893263";
						show1 = "name_10397445893263";
						CardLayout cl = (CardLayout) (centralPanel.getLayout());
						cl.show(centralPanel, show2);
					}
				});
				btCredits.setActionCommand("Cancel");
			}
			GridBagConstraints gbc_btCredits = new GridBagConstraints();
			gbc_btCredits.anchor = GridBagConstraints.NORTHWEST;
			gbc_btCredits.insets = new Insets(0, 0, 0, 5);
			gbc_btCredits.gridx = 1;
			gbc_btCredits.gridy = 0;
			buttonPane.add(btCredits, gbc_btCredits);
			GridBagConstraints gbc_btLlicencia = new GridBagConstraints();
			gbc_btLlicencia.anchor = GridBagConstraints.NORTHWEST;
			gbc_btLlicencia.insets = new Insets(0, 0, 0, 5);
			gbc_btLlicencia.gridx = 2;
			gbc_btLlicencia.gridy = 0;
			buttonPane.add(btLicence, gbc_btLlicencia);
			{
				btClose = new JButton("Close");
				btClose.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				btClose.setActionCommand("OK");
				getRootPane().setDefaultButton(btClose);
			}
			GridBagConstraints gbc_btTanca = new GridBagConstraints();
			gbc_btTanca.insets = new Insets(0, 0, 0, 5);
			gbc_btTanca.anchor = GridBagConstraints.NORTHWEST;
			gbc_btTanca.gridx = 4;
			gbc_btTanca.gridy = 0;
			buttonPane.add(btClose, gbc_btTanca);
		}
		// /////////////////////////////////77
		{
			iconPanel = new JPanel();
			iconPanel.setBounds(0, 0, 440, 86);
			contentPanel.add(iconPanel);
			iconPanel.setAlignmentY(Component.TOP_ALIGNMENT);
			{
				lblImage = new JLabel("");
				lblImage.setBounds(166, 5, 100, 100);
				lblImage.setIcon(new ImageIcon("images/ateneu_logo_100px.jpg"));
			}
			iconPanel.setLayout(null);
			iconPanel.add(lblImage);
			{

			}
			lblImage.setAlignmentX(CENTER_ALIGNMENT);
		}

	}
}
