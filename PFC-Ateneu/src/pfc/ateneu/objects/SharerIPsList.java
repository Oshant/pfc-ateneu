package pfc.ateneu.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * List of SharerIPs that contains all the IPs of each file we want to download
 * 
 * Llista de SharersIPs que cont� totes les IPs de cada fitxer que volem descarregar
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class SharerIPsList implements Serializable {
	List<SharersIPs> listOfFilesToDownload;

	public SharerIPsList() {
		this.listOfFilesToDownload = new ArrayList<SharersIPs>();
	}

	public SharerIPsList(List<SharersIPs> listOfFilesToDownload) {
		this.listOfFilesToDownload = listOfFilesToDownload;
	}

	public List<SharersIPs> getListOfFilesToDownload() {
		return listOfFilesToDownload;
	}

	public void setFileInList(SharersIPs sharer) {
		listOfFilesToDownload.add(sharer);
	}

	public void setListOfFilesToDownload(List<SharersIPs> listOfFilesToDownload) {
		this.listOfFilesToDownload = listOfFilesToDownload;
	}

	@Override
	public String toString() {
		return "SharerIPsList [listOfFilesToDownload=" + listOfFilesToDownload
				+ "]";
	}

}
