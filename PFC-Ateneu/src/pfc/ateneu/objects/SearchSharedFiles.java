package pfc.ateneu.objects;

import java.io.Serializable;
import java.util.List;
/**
 * This object is sent to the server with a string of what you are searching and the server sends it back with the list of files
 * that fit to the string
 * 
 * Objecte enviat al servidor amb el fitxer demanat, aquest torna amb la llista de Shared files com a resposta
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class SearchSharedFiles implements Serializable{
	String nameToMatch;
	List<SharedFile> files;
	public SearchSharedFiles(String nameToMatch) {
		this.nameToMatch = nameToMatch;
	}

	public String getNameToMatch() {
		return nameToMatch;
	}

	public void setNameToMatch(String nameToMatch) {
		this.nameToMatch = nameToMatch;
	}
	
	public List<SharedFile> getFiles() {
		return files;
	}

	public void setFiles(List<SharedFile> files) {
		this.files = files;
	}
}
