package pfc.ateneu.objects;

import java.io.Serializable;
import java.util.Arrays;

import pfc.ateneu.main.AteneuMain;
import pfc.ateneu.main.Globals;

/**
 * This object will be used in order to request an specific piece of an specific file in the P2P comunication
 * 
 * Pe�a clau en l'enviament d'informacio entre els clients, cont� la
 * informaci� del fitxer que s'est� enviant, el id d'aquest per poder-lo
 * identificar i la posicio on va aquesta informaci�
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class Piece implements Serializable {

	long idFile;
	int numPiece;
	String hash;
	byte[] data;

	public Piece() {
		data = new byte[Globals.dataSize];
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public long getIdFile() {
		return idFile;
	}

	public void setIdFile(long idFile) {
		this.idFile = idFile;
	}

	public int getNumPiece() {
		return numPiece;
	}

	public void setNumPiece(int numPiece) {
		this.numPiece = numPiece;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Piece [idFile=" + idFile + ", numPiece=" + numPiece + ", hash="
				+ hash + ", data=" + Arrays.toString(data) + "]";
	}

}
