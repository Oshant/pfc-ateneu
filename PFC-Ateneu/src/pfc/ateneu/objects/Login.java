package pfc.ateneu.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * This object will be used to login. We use our MAC address and a list of the files we have added or deleted
 * and another list for retrieving information about this files
 * 
 * Gesti� de l'alta de l'usuari si escau i la verificaci� al servidor per comen�ar a compartir fitxers. Amb una llista de fitxers 
 * afegits o esborrats i una altra per obtenir la informaci� d'aquests fitxers.
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class Login implements Serializable{
	String mac;
	List<SharedFile> files;
	List<SharedFile> filesWeAlreadyHave;
	public Login(String mac, List<SharedFile> files) {
		this.mac = mac;
		this.files = files;
		filesWeAlreadyHave = new ArrayList<SharedFile>();
	}
	public String getMac() {
		return mac;
	}
	public List<SharedFile> getFiles() {
		return files;
	}
	public List<SharedFile> getFilesWeAlreadyHave() {
		return filesWeAlreadyHave;
	}
	
	public void setFilesWeAlreadyHave(List<SharedFile> filesWeAlreadyHave) {
		this.filesWeAlreadyHave = filesWeAlreadyHave;
	}
	public void setFilesWeAlreadyHave(SharedFile fileWeAlreadyHave) {
		filesWeAlreadyHave.add(fileWeAlreadyHave);
	}
	
	
}
