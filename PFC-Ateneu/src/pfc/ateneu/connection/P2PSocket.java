package pfc.ateneu.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import pfc.ateneu.main.AteneuMain;
import pfc.ateneu.main.Globals;
import pfc.ateneu.objects.Piece;
import pfc.ateneu.p2ppackage.FileManagement;


/**
 * Class to manage the connection with another peer and sharing files with it.
 * 
 * Classe per mantenir una connexi� amb un altre client i compartir fitxers amb aquest client
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class P2PSocket implements Runnable {
	ObjectInputStream in = null;
	ObjectOutputStream out = null;
	Socket socket = null;
	boolean keepConnection = true, receiver;
	int socketFails = 0; // if socket has failed 10 times without stop we close
							// this thread
	FileManagement fileManagement;
	Piece piece = null;
	long id; 
	/**
	 * @param fileManagement file manager
	 * @param address address we connect to
	 * @param id if of the file we are sharing
	 * 
	 * @param fileManagement gestor de fitxers
	 * @param address adressa a la que contectar-se
	 * @param id id del fitxer de desc�rrega
	 */
	public P2PSocket(FileManagement fileManagement, InetAddress address, long id) {
		try {
			System.out.println(id+" iddd");
			this.socket = new Socket(address, Globals.portClient);
		} catch (IOException e) {
			System.out.println("petaaa........................");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.id = id;
		this.receiver = true;
		this.fileManagement = fileManagement;
		System.out.println("obrint p2p");
	}

	@Override
	public void run() {
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());

			while (keepConnection) {
				try {
					if (receiver) { 
						System.out.println("sasdasdasdasdasd");
						piece = new Piece();
						int pie=fileManagement.getNextPiece(id);
						System.out.println("seguent peçaaaa : "+pie);
						if(pie == -2) 
							break;
						piece.setNumPiece(pie);
						
						System.out.println("1111");
						System.out.println("2222");
						System.out.println("3333");
						piece.setIdFile(id);
						System.out.println("4444");

						System.out.println("5555");
						out.writeObject(piece);
						System.out.println(piece.toString());
						
						System.out.println("6666");
						piece = (Piece) in.readObject();
						//System.out.println(piece.toString());
						System.out.println("7777");
						// TOT AIXÒ ENCARA S'HA D'IMPLEMENTAR!
						// fileManagement.setIdToName(fileToDownload.getId(),
						// fileToDownload.getName());
						fileManagement.writeFile(piece);

						System.out.println("escribim "+piece.toString());
						// fileManagement.writeFile(piece);
						System.out
								.println("Rebent del client (som receptor): \n\t"
										+ piece.toString());
					} else {
						Piece piece = (Piece) in.readObject();
						System.out
								.println("Rebent del client(som emisors): \n\t"
										+ piece.toString());
						 piece = fileManagement.readFile(piece);
						out.writeObject(piece);
					} 
					if (socketFails != 0)
						socketFails = 0;
				} catch (IOException | ClassNotFoundException e) {
					socketFails++;
					if (socketFails >= 10)
						keepConnection = false; // CONNECTION IS LOST (END OF
												// THREAD)
					System.out.println(socketFails + "outtttttttt22222"
							+ receiver);
				}
			}
		} catch (IOException e) {
			System.out.println("____________");
		}
		try {// CLOSE
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("END OF THREAD (SocketAmbClient)");
	}

	// FileManagement
	/**
	 * 
	 * @param clientConnectat
	 * @param receiver
	 * @param fileManagement
	 */
	public P2PSocket(Socket clientConnectat, boolean receiver,
			FileManagement fileManagement) {
		this.socket = clientConnectat;
		this.receiver = receiver;
		this.fileManagement = fileManagement;
		System.out.println("obrint p2p listener");
	}

	public int getMissingPlace() {
		return 0;
	}

	public String getPieceFromPeer() {
		return null;
	}

	public void requestPiece(Piece piece) {

	}
}
