package pfc.ateneu.connection;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import pfc.ateneu.main.AteneuMain;
import pfc.ateneu.main.Globals;
import pfc.ateneu.p2ppackage.FileManagement;
/**
 * Management of the input connections
 * 
 * Control de les connexions entrants
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class ListenerSocket implements Runnable{
	FileManagement fileManagement;
	/**
	 * 
	 * @param fileManagement
	 */
	public ListenerSocket(FileManagement fileManagement) {
		this.fileManagement = fileManagement;
	}

	@Override
	public void run() {
		java.net.ServerSocket server = null;
		try {
			server = new java.net.ServerSocket(Globals.portClient);
			
			while(true) 
			{
				System.out.println("Esperant el client  ");
				Thread thread = new Thread(new P2PSocket(server.accept(), false, fileManagement));
				thread.start(); //WE OPEN A NEW SOCKET FOR THE CONNECTED CLIENT    
			}       
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
