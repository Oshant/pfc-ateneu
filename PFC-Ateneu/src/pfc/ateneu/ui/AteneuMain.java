package pfc.ateneu.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JTabbedPane;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.SliderUI;

import dialeg.Dialog;

import adapters.DownloadAdapter;
import adapters.SearchAdapter;
import pfc.ateneu.clientserver.PeerToServerSocket;
import pfc.ateneu.main.Globals;
import pfc.ateneu.objects.SearchSharedFiles;
import pfc.ateneu.objects.SharedFile;
import pfc.ateneu.p2ppackage.FileManagement;

public class AteneuMain extends JFrame implements ActionListener, ItemListener{
	//JPanel panelCard;
	DownloadPanel downloadPanel;
	ResultSearchPanel resultSearchPanel;
	public static Color backgroundColor;
	JTabbedPane tabbedPane;
	FileManagement fileManagement;
	SearchAdapter searchAdapter;
	DownloadAdapter downloadAdapter;
	
	private JScrollBar scrollBar;
	static int width = 985, height = 525, dataSize = 100000;

	 
	
	public AteneuMain(FileManagement fileManagement) {
		this.fileManagement = fileManagement;
		searchAdapter = new SearchAdapter();
		downloadAdapter = new DownloadAdapter();
		SearchAdapter searchAdapter = new SearchAdapter();
		PeerToServerSocket toServer = new PeerToServerSocket(fileManagement, searchAdapter, downloadAdapter);
		Thread thread = new Thread(toServer);
		thread.start();  
		ImageIcon img  = new ImageIcon("images/ateneu_logo_100px.jpg");
		setIconImage(img.getImage());
		
		JMenuBar menuBar = new JMenuBar();

		//Build the first menu.
		JMenu menu = new JMenu("About");
		menu.setMnemonic(KeyEvent.VK_A);
//		menu.getAccessibleContext().setAccessibleDescription(
//		        "The only menu in this program that has menu items");
		

		
		JMenuItem menuItem = new JMenuItem("About us",
		                         KeyEvent.VK_T);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		menuBar.add(menu);
		setJMenuBar(menuBar);
		
		backgroundColor = new Color(235, 236, 243);
		JPanel contentPanel = new JPanel();
		contentPanel.setBounds(0, 0, 1000, 630);
		getContentPane().add(contentPanel);
		

		
		contentPanel.setLayout(null); 
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1000, 630);
		System.out.println("__"+getWidth());
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {50, 30, 50, 30, 30, 5};
		gridBagLayout.rowHeights = new int[] {20,28, 500};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{1.0, 1.0};
		contentPanel.setLayout(gridBagLayout);
		contentPanel.setBackground(backgroundColor);
		
		ButtonsPanel buttonPanel = new ButtonsPanel(this, searchAdapter);
		buttonPanel.setBackground(new Color(0,0,0,0));
		GridBagConstraints gbc_buttonsPanel = new GridBagConstraints();
		gbc_buttonsPanel.weighty = 1.0;
		gbc_buttonsPanel.weightx = 1.0; 
		gbc_buttonsPanel.fill = GridBagConstraints.BOTH;
		gbc_buttonsPanel.insets = new Insets(0, 0, 0, 0);
		gbc_buttonsPanel.gridx = 0; 
		gbc_buttonsPanel.gridy = 1; 
		

		
		SearchSharedFiles ssf = new SearchSharedFiles("hola");
		
		
		
		
		downloadPanel = new DownloadPanel(fileManagement, downloadAdapter);
		//downloadPanel.setSharedFiles(ssf);
		Thread thread2 = new Thread(downloadPanel);
		thread2.start(); 
		
		resultSearchPanel = new ResultSearchPanel(searchAdapter, fileManagement);
		
		JLabel lblSignboard = new JLabel("");
		GridBagConstraints gbc_lblSignboard = new GridBagConstraints();
		gbc_lblSignboard.anchor = GridBagConstraints.WEST;
		gbc_lblSignboard.insets = new Insets(0, 0, 5, 5);
		gbc_lblSignboard.gridx = 2;
		gbc_lblSignboard.gridy = 1;
		lblSignboard.setIcon(new ImageIcon("images/ateneu_signboard.png"));
		contentPanel.add(lblSignboard, gbc_lblSignboard); 
		//resultSearchPanel.setSharedFiles(ssf);
		
		SearchPanel searchPanel = new SearchPanel(resultSearchPanel, this, searchAdapter );
		searchPanel.setBackground(new Color(0,0,0,0));
		GridBagConstraints gbc_searchPanel = new GridBagConstraints();
		gbc_searchPanel.gridwidth = 2;
		gbc_searchPanel.weighty = 1.0;
		gbc_searchPanel.weightx = 1.0; 
		gbc_searchPanel.fill = GridBagConstraints.BOTH;
		gbc_searchPanel.insets = new Insets(0, 0, 5, 0);
		gbc_searchPanel.gridx = 3; 
		gbc_searchPanel.gridy = 1; 
		contentPanel.add(searchPanel, gbc_searchPanel);

		

		GridBagConstraints gbc_cardLayout = new GridBagConstraints();
		gbc_cardLayout.weighty = 1.0;
		gbc_cardLayout.weightx = 1.0;
		gbc_cardLayout.insets = new Insets(0, 0, 0, 0);
		gbc_cardLayout.fill = GridBagConstraints.BOTH;
		gbc_cardLayout.gridx = 0;
		gbc_cardLayout.gridy = 2;
		gbc_cardLayout.gridwidth = 2;
	
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, width, height);
		tabbedPane.addTab("Downloads", null, downloadPanel, null);
		tabbedPane.addTab("Search", null, resultSearchPanel, null);
		
		GridBagConstraints pannedLayout = new GridBagConstraints();
		pannedLayout.weighty = 1.0;
		pannedLayout.weightx = 1.0;
		pannedLayout.insets = new Insets(0, 0, 0, 0);
		pannedLayout.fill = GridBagConstraints.BOTH;
		pannedLayout.gridx = 0; 
		pannedLayout.gridy = 2;
		pannedLayout.gridwidth = 6;
		contentPanel.add(tabbedPane, pannedLayout,1);
		
	}
 
	public void setSearch(){
		tabbedPane.setSelectedIndex(1);
	}


	@Override
	public void itemStateChanged(ItemEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Dialog llw = new Dialog();
		llw.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		llw.setVisible(true); 
		llw.show();  
	}
}