package pfc.ateneu.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

import pfc.ateneu.main.Globals;
import pfc.ateneu.objects.SearchSharedFiles;
import pfc.ateneu.objects.SharedFile;
import pfc.ateneu.p2ppackage.FileManagement;

import javax.swing.JScrollBar;
import javax.swing.table.DefaultTableCellRenderer;

import adapters.DownloadAdapter;

public class DownloadPanel extends JPanel implements Runnable{
	List<DownloadFile> files = null;
	final String name = "Name", downloaded = "Downloaded", totalSize = "Total size", downloadSpeed = "Download Speed";
	final int KiloByte = 1024, MegaByte = KiloByte * KiloByte, GigaByte = MegaByte * KiloByte;
	FileManagement fileManagement;
	DownloadAdapter downloadAdapter;
	List<SharedFile> sharedFiles;
	/**
	 * Create the panel.
	 */
	
	public void setSharedFiles(SearchSharedFiles searchSharedFiles)
	{
		
				String[] columnNames = {name, downloaded, totalSize, downloadSpeed};
				List<SharedFile> sharedFiles = searchSharedFiles.getFiles();
				sharedFiles.addAll(this.sharedFiles);
				this.sharedFiles = sharedFiles;
				Object[][] data = new Object[300][300];
				files = new ArrayList<DownloadFile>();
				int cont = 0;
				for(SharedFile shared: sharedFiles)
				{
					
					DownloadFile downloadFile = new DownloadFile(shared.getName(), shared.getSize(), fileManagement);
					files.add(downloadFile);
					data[cont][0] = new String(downloadFile.getName());
					int downloaded = downloadFile.getDownloaded();
					if(downloaded < MegaByte)
						data[cont][1] = new String((downloadFile.getDownloaded()/KiloByte)+" KB");
					else if(downloaded < GigaByte)
						data[cont][1] = new String((downloadFile.getDownloaded()/MegaByte)+" MB");
					
					long size = downloadFile.getFileSize();
					if(size < KiloByte)
						data[cont][2] = new String(downloadFile.getFileSize()+" Bytes");
					else if(size < MegaByte)
						data[cont][2] = new String((downloadFile.getFileSize()/KiloByte)+" KB");
					else if(size < GigaByte)
						data[cont][2] = new String((downloadFile.getFileSize()/MegaByte)+" MB");
					else
						data[cont][2] = new String((downloadFile.getFileSize()/GigaByte)+" GB");
					data[cont][3] = new String(downloadFile.getDownloadSpeed()+" KB/s");
					cont++;
				}

				//table
				JTable table = new JTable(data, columnNames);
				table.setFillsViewportHeight(true);
				table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
				table.setPreferredSize(new Dimension(AteneuMain.width, AteneuMain.height));
				DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
				centerRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
				table.getColumn(name).setCellRenderer( centerRenderer );
				table.getColumn(downloaded).setCellRenderer( centerRenderer );
				table.getColumn(totalSize).setCellRenderer( centerRenderer );
				table.getColumn(downloadSpeed).setCellRenderer( centerRenderer );
				table.setShowGrid(false);
				table.setEnabled(false); //JTable won't be editable
				
				table.addMouseListener(new MouseAdapter() {
				    public void mousePressed(MouseEvent me) {
				        JTable table =(JTable) me.getSource();
				        Point p = me.getPoint();
				        int row = table.rowAtPoint(p);
				        if (me.getClickCount() == 2 && row < files.size()) {
				            //In here we detect the double clicked file.
				        	System.out.println(row+"");
				        	
				        }
				    }
				});
				//scroll pane
				JScrollPane scrollPane = new JScrollPane(table);
				scrollPane.setPreferredSize(new Dimension(AteneuMain.width, AteneuMain.height));
				scrollPane.setViewportBorder(null);
				setVisible(false);
				add(scrollPane);
				setVisible(true);
				scrollPane.repaint();  
				System.out.println(table.getRowCount());
		
	}
	public DownloadPanel(FileManagement fileManagement, DownloadAdapter downloadAdapter) {
		this.fileManagement = fileManagement;
		this.downloadAdapter = downloadAdapter;
		sharedFiles = new ArrayList<SharedFile>();
		setBackground(AteneuMain.backgroundColor);
		setVisible(true);
		setPreferredSize(new Dimension(AteneuMain.width, AteneuMain.height));
	
	}
	
	public void updateDownloadInfo(){
		if(files !=null){
			try {
				Thread.sleep(2222);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	for(DownloadFile f : files){
		try {
			f.setDownloaded();
			System.out.println(f.getName());
			System.out.println(" dess "+f.getDownloaded());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
		}
		//fileManagement.getDownloaded(name)
//		File dir = new File(Globals.sharedDirectory+Globals.notFinishedURI);
//		File[] listFiles = dir.listFiles();
//		for(DownloadFile df: files)
//		{
//			for(File f: listFiles)
//			{
//				try {
//					fileManagement.getDownloaded(f.getName());
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
	}
	
	@Override
	public void run() {
		while(true)
		{
			
			List<SharedFile> shared = new ArrayList<SharedFile>();

			if(downloadAdapter.getFilesDownloadPanel() != null)
			{
				for(SharedFile file: downloadAdapter.getFilesDownloadPanel())
				{
					shared.add(new SharedFile(file.getName(), file.getMd5(), file.getId(),
					file.getNumberOfSharers(), file.getSize()));

				}
			
				SearchSharedFiles sShared = new SearchSharedFiles("");
				sShared.setFiles(shared);
				setSharedFiles(sShared);
				downloadAdapter.setFilesDownloadPanel(null);
			}
			updateDownloadInfo();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
