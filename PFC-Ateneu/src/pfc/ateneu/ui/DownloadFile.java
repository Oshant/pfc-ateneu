package pfc.ateneu.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JPanel;

import pfc.ateneu.p2ppackage.FileManagement;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.JScrollBar;

public class DownloadFile extends JPanel{
	String name;
	//downloa
	int downloaded, downloadSpeed;
	long size;
	FileManagement fileManagement;
	
	public DownloadFile(String name, long size, FileManagement fileManagement) {
		this.name = name;
		this.size = size;
		this.fileManagement = fileManagement;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getFileSize() {
		return size;
	}
	public void setFileSize(int size) {
		this.size = size;
	}
	public int getDownloaded() {
		return downloaded;
	}
	public void setDownloaded() throws IOException {
		int downloaded = fileManagement.getDownloaded(name);
		downloaded = (downloaded * AteneuMain.dataSize) / 1024;
		downloadSpeed = downloaded - this.downloaded;
		this.downloaded = downloaded;
	}
	public int getDownloadSpeed() {
		return downloadSpeed;
	}
	
}
