package pfc.ateneu.ui;

import java.awt.Color;

import pfc.ateneu.connection.ListenerSocket;
import pfc.ateneu.p2ppackage.FileManagement;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FileManagement fm = new FileManagement(); 
		AteneuMain am = new AteneuMain(fm); 
		am.setVisible(true);  
		new Thread(new ListenerSocket(fm)).start();
	}                       
}                         
                                            