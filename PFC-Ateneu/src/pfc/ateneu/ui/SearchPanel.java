package pfc.ateneu.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;

import pfc.ateneu.objects.SearchSharedFiles;
import adapters.SearchAdapter;

public class SearchPanel extends JPanel{
	private JTextField txtSearch;
	public ResultSearchPanel results;
	SearchAdapter searchAdapter;
	AteneuMain main;
	/**
	 * Create the panel.
	 */
	public SearchPanel(ResultSearchPanel results, final AteneuMain main, final SearchAdapter searchAdapter) {
		this.searchAdapter = searchAdapter;
		this.main = main;
		this.results = results;
		setLayout(null);
		setBounds(0, 0, 340, 30);
		txtSearch = new JTextField();
		txtSearch.addActionListener(new ActionListener() {

		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	main.setSearch();
				searchAdapter.setSearchSharedFiles(new SearchSharedFiles(txtSearch.getText()));
		    }
		});
		txtSearch.setBounds(0, 0, 200, 24);
		add(txtSearch);
		txtSearch.setColumns(10);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				main.setSearch();
				searchAdapter.setSearchSharedFiles(new SearchSharedFiles(txtSearch.getText()));
			}
		});
		btnSearch.setBounds(201, 0, 99, 24);
		add(btnSearch);

	}
}
