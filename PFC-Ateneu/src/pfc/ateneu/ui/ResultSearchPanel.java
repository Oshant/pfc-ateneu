package pfc.ateneu.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.plaf.SliderUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import adapters.SearchAdapter;
import pfc.ateneu.objects.SearchSharedFiles;
import pfc.ateneu.objects.SharedFile;
import pfc.ateneu.p2ppackage.FileManagement;

public class ResultSearchPanel extends JPanel {
	final String name = "Name", size = "Size", sharers = "Number of sharers";
	SearchSharedFiles searchSharedFiles;
	List<SharedFile> sharedFiles;
	FileManagement fileManagement;
	final int KiloByte = 1024, MegaByte = KiloByte * KiloByte, GigaByte = MegaByte * KiloByte;
	/**
	 * Create the panel.
	 */
	public ResultSearchPanel(SearchAdapter searchAdapter, FileManagement fileManagement) {
		this.fileManagement = fileManagement;
		setBackground(AteneuMain.backgroundColor);
		setPreferredSize(new Dimension(AteneuMain.width, AteneuMain.height));
//		FlowLayout flowLayout = new FlowLayout();
//		flowLayout.setAlignment(FlowLayout.LEFT);
//		setLayout(flowLayout);
		
		(new Thread(new ResultsRunnable(searchAdapter))).start();
		
	}
	
	public class ResultsRunnable implements Runnable {
		SearchAdapter searchAdapter;
		public ResultsRunnable(SearchAdapter searchAdapter){
			this.searchAdapter = searchAdapter;
		}
	    public void run() {
	    	while(true){
	    		try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

	    		if(searchAdapter.getFilesAvailableToDownload() != null){
	    			setSharedFiles(searchAdapter.getFilesAvailableToDownload());
	    			setVisible(true);
	    			System.out.println("num elements "+searchSharedFiles.getFiles().size()+isVisible()+countComponents());
	    			searchAdapter.setFilesAvailableToDownload(null);
	    		}
	    	}
	    }

	}
	        
	
	public void setSharedFiles(SearchSharedFiles searchSharedFiles)
	{
		System.out.println("---------------------"+searchSharedFiles.getFiles().size());
		this.searchSharedFiles = searchSharedFiles;
		sharedFiles = searchSharedFiles.getFiles();
		String[] columnNames = {name, size, sharers};

		Object[][] data = new Object[300][300];
		int cont = 0;
		for(SharedFile shared: sharedFiles)
		{
			data[cont][0] = new String(shared.getName());
			long size = shared.getSize(); 
			if(size < KiloByte)
				data[cont][1] = new String(shared.getSize()+" Bytes");
			else if(size < MegaByte)
				data[cont][1] = new String((shared.getSize()/KiloByte)+" KB");
			else if(size < GigaByte)
				data[cont][1] = new String((shared.getSize()/MegaByte)+" MB");
			else 
				data[cont][1] = new String((shared.getSize()/GigaByte)+" GB");
			data[cont][2] = new String(shared.getNumberOfSharers()+"");
			cont++;
		}
		
 
		//table
		JTable table = new JTable(data, columnNames);
		
		table.setFillsViewportHeight(true);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setPreferredSize(new Dimension(AteneuMain.width, AteneuMain.height));
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
		
		table.getColumn(name).setCellRenderer( centerRenderer );
		table.getColumn(size).setCellRenderer( centerRenderer );
		table.getColumn(sharers).setCellRenderer( centerRenderer );
		table.setEnabled(false); //JTable won't be editable
		table.setShowGrid(false);
		
		table.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        if (me.getClickCount() == 2 && row < sharedFiles.size()) {
		            //In here we detect the double clicked file.
		        	System.out.println(row+"");
		        	System.out.println(sharedFiles.get(row).toString());
		        	fileManagement.prepareFileToDownload(sharedFiles.get(row));
		        }
		    }
		}); 
		
		//scroll pane
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(AteneuMain.width, AteneuMain.height));
		scrollPane.setViewportBorder(null);
		removeAll();
		setVisible(false);
		add(scrollPane);
		System.out.println(table.countComponents());
		if(table.countComponents()>0)
			System.out.println(table.getValueAt(0, 0));		
		setVisible(true);
		scrollPane.repaint();
		
	}

}
