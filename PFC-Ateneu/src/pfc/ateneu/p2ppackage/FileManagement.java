package pfc.ateneu.p2ppackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pfc.ateneu.main.Globals;
import pfc.ateneu.objects.Piece;
import pfc.ateneu.objects.SharedFile;

/**
 * Manages the write and read of files we are downloading or sending
 * 
 * Gestiona la escriptura y la lectura dels fitxers dintre de la xarxa de
 * relacions dels usuaris
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class FileManagement {

	List<Piece> pieces;
	FileOutputStream writer;
	FileInputStream reader;
	Hashtable<Long, String> idToName;
	JSonHandler jSonHandler;

	public FileManagement() {
		pieces = new ArrayList<Piece>();
		idToName = new Hashtable<Long, String>();
		jSonHandler = new JSonHandler();
	}

	public Long getIDFromName(String name) {
		System.out.println("name 2: " + name);
		Iterator<Map.Entry<Long, String>> it = idToName.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<Long, String> entry = it.next();
			System.out.println(entry.getValue());
			if (entry.getValue().equals(name))
				return entry.getKey();
		}
		return null;
	}

	/**
	 * Afegeix un fitxer a la llista de conversio de id to name
	 * 
	 * @param id
	 * @param name
	 */
	public void setIdToName(long id, String name) {
		idToName.put(id, name);
	}

	/**
	 * Comproba la consistencia del fitxers de desc�rrega
	 * 
	 * @throws IOException
	 */
	public void checkFileConsistence() throws IOException {
		File directory = new File(Globals.sharedDirectory);
		File[] llistaFiles = directory.listFiles();
		for (File file : llistaFiles) {
			if (file.getName().contains(Globals.notFinished)) {
				String fileName = file.getName().substring(0,
						file.getName().length() - Globals.notFinished.length());
				for (File notFinishedFile : llistaFiles) {
					if (fileName.equals(notFinishedFile)) {
						reader = new FileInputStream(Globals.sharedDirectory
								+ fileName);
						byte[] data = new byte[Globals.dataSize];

						FileInputStream readerChecker = new FileInputStream(
								file);
						int value, numberOfPiece = 0;
						while ((value = readerChecker.read()) != -1) {
							reader.read(data, numberOfPiece * Globals.dataSize,
									Globals.dataSize);
							String dataToCheck = new String(data);
							// if((char)value == 0 && dataToCheck.)

							numberOfPiece++;
						}
						try {
							if (readerChecker != null)
								readerChecker.close();
							if (reader != null)
								reader.close();
						} catch (IOException ex) {
							ex.printStackTrace();
						}

						break;
					}
				}
			}
		}
	}

	public void generateNewFileOfFileListDifferences() {
		// generem un nou xml (l'usuari és nou o fitxer corrupte)
	}

	/**
	 * 
	 * @return retorna una llista amb les diferencies entre la llista nova i la
	 *         vella
	 */
	public List<SharedFile> getFileListDiferences() {
		List<SharedFile> listOfCreatedAndDeletedFiles = new ArrayList<SharedFile>();

		try {
			listOfCreatedAndDeletedFiles = jSonHandler.getNewFilesInList();
			for (SharedFile list : listOfCreatedAndDeletedFiles)
				System.out.println("llista nous -> " + list.getName() + " ");

			listOfCreatedAndDeletedFiles.addAll(jSonHandler.getDeletedFiles());
			System.out.println("-----" + listOfCreatedAndDeletedFiles.size());
			for (SharedFile list : listOfCreatedAndDeletedFiles)
				System.out.println("llista creats i borrats -> "
						+ list.getName() + " " + list.isNew());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listOfCreatedAndDeletedFiles;
	}

	/**
	 * Llista modificada del servidor, si s'han introduit fitxers nous al
	 * sistema aquets inclouran la id asociada
	 * 
	 * @param modifiedInServer
	 */
	public void saveList(List<SharedFile> modifiedInServer) {
		List<SharedFile> oldList = jSonHandler.getOldList();
		List<SharedFile> newList = new ArrayList<SharedFile>();
		for (SharedFile modified : modifiedInServer) {
			System.out.println("fitxer afegit:" + modified.toString());
			if (modified.isNew())
				newList.add(modified);
			else {
				for (int i = 0; i < oldList.size(); i++) {
					if (oldList.get(i).getName().contains("NotFinished")
							|| oldList.get(i).getName()
									.equals(modified.getName())
							&& oldList.get(i).getMd5()
									.equals(modified.getMd5())) {
						oldList.remove(i);
					}
				}
			}
		}
		newList.addAll(oldList);
		jSonHandler.saveList((ArrayList<SharedFile>) newList);
		for (SharedFile file : newList) {
			setIdToName(file.getId(), file.getName());
		}
	}
	/**
	 * Escriu la pe�a i actualitza el fitxer de control
	 * 
	 * @param piece
	 */
	public synchronized void writeFile(Piece piece) {
		PrintWriter writerChecker;
		// ///::..WE WRITE IN FILE..:://///
		System.out.println("________________WRITE IN FILE____________");
		System.out.println("getidfile: "
				+ piece.getIdFile()
				+ " idtoname: "
				+ idToName.get(piece.getIdFile())
				+ " "
				+ Paths.get(Globals.sharedDirectory + Globals.notFinishedURI
						+ idToName.get(piece.getIdFile())) + " pos "
				+ piece.getNumPiece() * Globals.dataSize + " DATA "
				+ piece.getData());

		Path p = Paths.get(Globals.sharedDirectory
				+ idToName.get(piece.getIdFile()));
		SeekableByteChannel sbc;

		System.out.println(p);
		try {
			sbc = Files.newByteChannel(p, StandardOpenOption.WRITE);
			sbc.position(piece.getNumPiece() * Globals.dataSize);
			ByteBuffer bb = ByteBuffer.wrap(piece.getData());
			sbc.write(bb);
			sbc.close();

			// //////////////////////////////////////////
			// ///::..WE WRITE IN THE CHECK FILE..:://///
			File file = new File(Globals.sharedDirectory
					+ Globals.notFinishedURI + idToName.get(piece.getIdFile())
					+ Globals.notFinished);
			FileInputStream readerChecker = new FileInputStream(file);
			int value;
			String data = "";
			while ((value = readerChecker.read()) != -1) {
				data += (char) value;
			}
			try {
				if (readerChecker != null)
					readerChecker.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			System.out
					.println("- - - - - - - - - - - - - - - - - - - - - - - -- - - - - - -- - -");
			System.out.println(data);
			data = data.substring(0, piece.getNumPiece()) + "1"
					+ data.substring(piece.getNumPiece() + 1);
			System.out.println(data);
			writerChecker = new PrintWriter(file, "UTF-8");
			writerChecker.write(data);
			writerChecker.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// //////////////////////////////////////////
	}
	/**
	 * Llegeix del fitxer segons la id que es troba a la pe�a i l'ompla amb la seccio pertinent
	 * @param piece
	 * @return
	 */
	public synchronized Piece readFile(Piece piece) {
		String fileName = idToName.get(piece.getIdFile());
		int bufferSize;
		try {
			// ///::..WE READ THE SHARED FILE..:://///
			reader = new FileInputStream(Globals.sharedDirectory + fileName);
			Path path = Paths.get(Globals.sharedDirectory + fileName);
			byte[] data;
			if ((piece.getNumPiece() * Globals.dataSize) + Globals.dataSize < Files
					.size(path)) {
				bufferSize = Globals.dataSize;
			} else {
				bufferSize = (int) (Files.size(path) - (piece.getNumPiece() * Globals.dataSize));
			}
			data = new byte[bufferSize];
			reader.getChannel()
					.position(piece.getNumPiece() * Globals.dataSize);
			for (int i = 0; i < bufferSize; i++) {
				if ((data[i] = (byte) reader.read()) != -1)
					;
			}
			piece.setData(data);

			if (reader != null)
				reader.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return piece;
		// ///////////////////////////////////////
	}

	public synchronized int getDownloaded(String name) throws IOException {
		File file = new File(Globals.sharedDirectory + Globals.notFinishedURI
				+ name + Globals.notFinished);
		FileInputStream readerChecker = new FileInputStream(file);
		int value, downloaded = 0;
		while ((value = readerChecker.read()) != -1) {
			System.out.print("downlaoded size: " + " " + value);
			if ((char) value == 1)
				downloaded++;
		}
		try {
			if (readerChecker != null)
				readerChecker.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return downloaded * Globals.dataSize;
	}

	public synchronized int getNextPiece(long id) throws IOException {
		System.out.println("id to name: *-**************************"
				+ idToName.get(id));
		File file = new File(Globals.sharedDirectory + Globals.notFinishedURI
				+ idToName.get(id) + Globals.notFinished);
		System.out.println(file.getAbsolutePath());
		FileInputStream readerChecker = new FileInputStream(file);
		int missingPiece = 0, value;
		while ((value = readerChecker.read()) != -1) {
			System.out.println("downlaoded piece: " + missingPiece + " "
					+ (char) value);
			if ((char) value == '0') {// if value is 0 (ASCII)
				System.out.println("sortim " + value);
				return missingPiece; // WE RETURN THE NEXT PIECE TO DOWNLOAD
			}
			missingPiece++;
		}
		try {
			if (readerChecker != null)
				readerChecker.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return -2; // FILE DOWNLOADED!

	}

	public void prepareFileToDownload(SharedFile fileToDownload) {
		PrintWriter writerChecker;
		// ///::..WE GENERATE THE CHECK FILE..:://///
		File file = new File(Globals.sharedDirectory + fileToDownload.getName());

		try {
			file.createNewFile();

			Path p = Paths.get(Globals.sharedDirectory + Globals.notFinishedURI
					+ fileToDownload.getName() + Globals.notFinished);

			System.out.println(p);
			try {

				byte[] blankBytes = new byte[(int) fileToDownload.getSize()];
				for (int i = 0; i < fileToDownload.getSize(); i++)
					blankBytes[i] = (byte) 0;
				System.out.println(blankBytes.length);
				FileOutputStream fop = new FileOutputStream(file);
				fop.write(blankBytes);
				fop.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		try {
			// file.createNewFile();
			File file2 = new File(Globals.sharedDirectory
					+ Globals.notFinishedURI + fileToDownload.getName()
					+ Globals.notFinished);
			writerChecker = new PrintWriter(file2, "UTF-8");// per fer(obtenir
															// num peces des del
															// xml)
			String zeros = "";
			for (int i = 0; i <= fileToDownload.getSize() / Globals.dataSize; i++) // WE
																					// FILL
																					// THE
																					// CHECK
																					// FILE
																					// WITH
																					// ZEROS
			{ // if we have no more data to write we exit this bucle
				if (fileToDownload.getSize() / Globals.dataSize == i
						&& fileToDownload.getSize() % Globals.dataSize == 0)
					break;
				zeros += "0";
			}
			writerChecker.write(zeros);
			writerChecker.close();

			// NOW, WE GENERATE THE FILE TO DOWNLOAD
			// FileWriter fw = new FileWriter(Globals.sharedDirectory
			// + fileToDownload.getName());
			// fw.close();

			// file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
