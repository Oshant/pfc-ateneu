package pfc.ateneu.p2ppackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pfc.ateneu.main.Globals;
import pfc.ateneu.objects.SearchSharedFiles;
import pfc.ateneu.objects.SharedFile;

import com.google.gson.Gson;

/**
 * Control of list of files. The ones we had, the ones we have and the ones we start downloading
 * 
 * Control de les llistes de fitxers, els que es tenien, els que estenen i els
 * que es comencen a descārregar
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class JSonHandler {

	public JSonHandler() {

	}

	public ArrayList<SharedFile> getNewFilesInList() throws IOException {
		ArrayList<File> files = getFilesInDirectory();
		ArrayList<SharedFile> filesShared = getOldList();
		ArrayList<SharedFile> newFiles = new ArrayList<SharedFile>();

		for (File f : files)
			System.out.println("FITXER DIRECTORI: " + f.getName());
		for (SharedFile sh : filesShared)
			System.out.println("FITXER ANTIC: " + sh.getName());
		for (File f : files) {
			if (f.getName().contains("~")
					|| f.getName().equals(Globals.listFiles) || f.isDirectory())
				continue;
			boolean isNew = true;
			for (SharedFile sh : filesShared) {
				if (sh.getName().equals(f.getName())) {
					isNew = false; // the file is registered in the list,
									// therefore this file is not new
					break;
				}
			}
			if (isNew) {
				System.out.println("===========> " + f.getName());
				newFiles.add(new SharedFile(f.getName(), getMD5(f
						.getAbsolutePath()), 0, Long.parseLong(""
						+ Files.size(f.toPath())), true));
			}
		}
		return newFiles;
	}
	/**
	 * 
	 * @return llista dels fitxers que han sigut borrats per poguer actuaitzar la informaciķ al servidor
	 */
	public ArrayList<SharedFile> getDeletedFiles() {
		ArrayList<File> files = getFilesInDirectory();
		ArrayList<SharedFile> filesShared = getOldList();
		ArrayList<SharedFile> deletedFiles = new ArrayList<SharedFile>();
		for (SharedFile sh : filesShared) {
			boolean isDeleted = true;
			for (File f : files) {
				if (sh.getName().equals(f.getName())) {
					isDeleted = false;
					break;
				}
			}
			if (isDeleted) {
				sh.setNew(false);
				deletedFiles.add(sh);
			}
		}

		return deletedFiles;
	}
	/**
	 * 
	 * @return llista enmagatzemada al sistema
	 */
	public ArrayList<SharedFile> getOldList() {
		try {
			File file = new File(Globals.sharedDirectory + Globals.listFiles);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();
			String str = new String(data, "UTF-8");
			Gson g = new Gson();
			SearchSharedFiles shf = g.fromJson(str, SearchSharedFiles.class);
			// System.out.println(str);
			return (ArrayList<SharedFile>) shf.getFiles();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// if something goes wrong we return null
		return null;
	}

	/**
	 * 
	 * @return llista amb els fitxers del directori
	 */
	public ArrayList<File> getFilesInDirectory() {
		File dir = new File(Globals.sharedDirectory);
		File[] listFiles = dir.listFiles();
		ArrayList<File> filesInDlirectory = new ArrayList<File>();
		for (File f : listFiles) {
			filesInDlirectory.add(f);
		}

		return filesInDlirectory;
	}
	/**
	 *  enmagatzema la llista en el directori predefinit
	 * @param newList llista a guardar
	 */
	public void saveList(ArrayList<SharedFile> newList) {
		System.out.println(newList.size());

		FileWriter fw;
		Gson gson = new Gson();
		String gSonString;
		for (SharedFile sh : newList)
			System.out.println(":::" + sh.getName());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");
		Date date = new Date();
		SearchSharedFiles shf = new SearchSharedFiles(dateFormat.format(date));
		shf.setFiles(newList);
		for (SharedFile sh : shf.getFiles())
			System.out.println("_ _ _" + sh.getName());
		try {
			fw = new FileWriter(Globals.sharedDirectory + Globals.listFiles);
			gSonString = gson.toJson(shf);
			fw.write(gSonString);
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/************************************************* MD5 functions ************************************************************************/
	private String getMD5(String filename) {
		try {
			return getMD5Checksum(filename);
		} catch (Exception e) {
			// if return null md5 can't be completed
			return null;
		}
	}

	private static String getMD5Checksum(String filename) throws Exception {
		byte[] b = createChecksum(filename);
		String result = "";

		for (int i = 0; i < b.length; i++) {
			result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}

	private static byte[] createChecksum(String filename) throws Exception {
		InputStream fis = new FileInputStream(filename);

		byte[] buffer = new byte[1024];
		MessageDigest complete = MessageDigest.getInstance("MD5");
		int numRead;

		do {
			numRead = fis.read(buffer);
			if (numRead > 0) {
				complete.update(buffer, 0, numRead);
			}
		} while (numRead != -1);

		fis.close();
		return complete.digest();
	}

}
