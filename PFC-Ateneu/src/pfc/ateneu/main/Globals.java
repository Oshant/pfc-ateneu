package pfc.ateneu.main;

import java.awt.Color;
/**
 * Globals variables (connections and paths)
 * 
 * Variables globals de conexi� i rutes
 * 
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class Globals {
	public static final int portServidor = 6010; 
	public static final int portClient = 6011; 
	public static final int dataSize = 10000;
	public static final String ipServer = "localhost"; 
	public static final String notFinished = "_NotFinished";
	public static final String notFinishedURI = "NotFinished/";
	public static final String sharedDirectory = "AteneuShared/";
	public static final String listFiles = "ListFiles.an";
}
