package pfc.ateneu.clientserver;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.plaf.SliderUI;

import adapters.DownloadAdapter;
import adapters.SearchAdapter;
import pfc.ateneu.connection.P2PSocket;
import pfc.ateneu.main.AteneuMain;
import pfc.ateneu.main.Globals;
import pfc.ateneu.objects.Login;
import pfc.ateneu.objects.Piece;
import pfc.ateneu.objects.SearchSharedFiles;
import pfc.ateneu.objects.SharedFile;
import pfc.ateneu.objects.SharerIPsList;
import pfc.ateneu.objects.SharersIPs;
import pfc.ateneu.p2ppackage.FileManagement;
import pfc.ateneu.p2ppackage.JSonHandler;

/**
 * Management of the connection with the server, makes the updates of the list of files
 * in the server and retrieves the new files and IPs of peers  
 * 
 * Control de la conexio amb el Servidor, permet la actualitzaci� de la llista
 * de fitxers de l'usuari al servidor, i posar-se amb contacte amb ell per
 * trobar nous fitxers i ip's
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class PeerToServerSocket implements Runnable {
	ObjectInputStream in = null;
	ObjectOutputStream out = null;
	Socket socket = null;
	boolean keepConnection = true, loggedIn = false, needIps = true;
	int socketFails = 0; // if socket has failed 10 times without stop we close
							// this thread
	FileManagement fileManagement;
	SearchAdapter searchAdapter;
	DownloadAdapter downloadAdapter;

	/**
	 * * Setting of the instance
	 * 
	 * @param fileManagement
	 * @param searchAdapter
	 *            Instance of SearchAdapter to allow the search
	 * @param downloadAdapter
	 *            Instanc eof DownloadAdapter to allow finding information of the downloads
	 *            
	 *            
	 * Preparaci� de la inst�ncia per poder dur a terme les seves operacions 
	 * 
	 * @param fileManagement
	 * @param searchAdapter
	 *            Instancia del Search Adapter per permetre la recerca
	 * @param downloadAdapter
	 *            Instancia del Download Adapter per permetre trobar informacio
	 *            de les desc�rregues
	 */
	public PeerToServerSocket(FileManagement fileManagement,
			SearchAdapter searchAdapter, DownloadAdapter downloadAdapter) {
		this.searchAdapter = searchAdapter;
		this.downloadAdapter = downloadAdapter;
		this.fileManagement = fileManagement;
		InetAddress address;
		try {
			System.out.println("::::::::");
			address = InetAddress.getByName(Globals.ipServer);
			this.socket = new Socket(address, Globals.portServidor);
			System.out.println("iniciem socket");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			System.out.println("111");
			out = new ObjectOutputStream(socket.getOutputStream());
			System.out.println("222");
			in = new ObjectInputStream(socket.getInputStream());
			System.out.println("333");

			while (keepConnection) {

				try {
					if (!loggedIn) {
						StringBuilder sb = new StringBuilder();
						try {
							Enumeration<NetworkInterface> networkInterfaces = NetworkInterface
									.getNetworkInterfaces();
							while (networkInterfaces.hasMoreElements()) {
								NetworkInterface network = networkInterfaces
										.nextElement();
								byte[] mac = network.getHardwareAddress();
								if (mac == null) {
									System.out.println("null mac");
								} else {
									for (int i = 0; i < mac.length; i++)
										sb.append(String
												.format("%02X%s",
														mac[i],
														(i < mac.length - 1) ? "-"
																: ""));
									break;
								}
							}
						} catch (SocketException e) {
							e.printStackTrace();
						}
						System.out.println(sb.toString());
						if (sb.toString() != null) {
							System.out.println("LOGIN!!!!!!!!!!!!!!!!!!");
							List<SharedFile> filesToDownload = new ArrayList<SharedFile>();
							for (SharedFile f : fileManagement
									.getFileListDiferences()) {
								System.out.println(Files.exists(Paths
										.get(Globals.sharedDirectory
												+ Globals.notFinishedURI
												+ f.getName()
												+ Globals.notFinished)));
								if (!Files.exists(Paths
										.get(Globals.sharedDirectory
												+ Globals.notFinishedURI
												+ f.getName()
												+ Globals.notFinished)))
									filesToDownload.add(f);
							}
							Login login = new Login(sb.toString(),
									filesToDownload);

							ArrayList<SharedFile> filesThatActuallyExist = new JSonHandler()
									.getOldList();
							for (int i = 0; i < login.getFiles().size(); i++) {
								SharedFile files = login.getFiles().get(i);
								if (files.isNew())
									filesThatActuallyExist.add(files);
								else {
									for (SharedFile f : filesThatActuallyExist)
										System.out.println("---> "
												+ f.getName());
									for (int j = 0; j < filesThatActuallyExist
											.size(); j++)
										if (filesThatActuallyExist.get(j)
												.getName()
												.equals(files.getName()))
											filesThatActuallyExist.remove(j);
									System.out.println("borrat "
											+ files.getName());
									for (SharedFile f : filesThatActuallyExist)
										System.out.println("---> "
												+ f.getName());
								}
							}
							for (SharedFile files : fileManagement
									.getFileListDiferences())
								System.out.println("fiii " + files.getName());

							for (SharedFile f : filesThatActuallyExist) {
								System.out.println("name: " + f.getName());
								fileManagement.setIdToName(f.getId(),
										f.getName());
								if (f.getId() == 0)
									break;

							}

							out.writeObject(login);
						} else {
							out.writeObject(new String("bye!"));
							keepConnection = false;
						}
						loggedIn = true;
					} else if (needIps) {
						System.out.println("NEED IPS");
						List<SharersIPs> listOfFilesToDownload = new ArrayList<SharersIPs>();
						File dir = new File(Globals.sharedDirectory
								+ Globals.notFinishedURI);
						File[] listFiles = dir.listFiles();
						for (SharedFile f : new JSonHandler().getOldList()) {
							for (File fil : listFiles) {
								System.out.println((fil.getName()) + " "
										+ f.getName() + Globals.notFinished);
								if ((fil.getName()).equals(f.getName()
										+ Globals.notFinished)) {
									System.out.println("------>"
											+ fil.getName());
									listOfFilesToDownload.add(new SharersIPs(f
											.getId()));
									break;
								}
							}
						}
						System.out.println("ABANS ENVIAR");
						SharerIPsList sil = new SharerIPsList(
								listOfFilesToDownload);
						out.writeObject(sil);
						System.out.println("despres ENVIAR");
					} else if (searchAdapter.getSearchSharedFiles() != null) {
						out.writeObject(searchAdapter.getSearchSharedFiles());
						searchAdapter.setSearchSharedFiles(null);
					} else {
						try {
							Thread.sleep(6000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						out.writeObject(new String("Still here!"));
					}

					// GETTING DATA
					socket.setSoTimeout(10000);
					Object obj = (Object) in.readObject();
					System.out.println("Rebent del server: \n\t"
							+ obj.toString());
					if (obj instanceof SearchSharedFiles) {
						SearchSharedFiles ssf = (SearchSharedFiles) obj;
						searchAdapter.setFilesAvailableToDownload(ssf);
						System.out.println(ssf.getFiles().size());
						ArrayList<SharedFile> sf = (ArrayList<SharedFile>) ssf
								.getFiles();
						JSonHandler jh = new JSonHandler();
						sf.addAll(jh.getOldList());
						for (SharedFile obtinguts : sf)
							System.out.println(obtinguts.getId() + " "
									+ obtinguts.getName());
						jh.saveList(sf);
					} else if (obj instanceof Login) {
						Login login = (Login) obj;
						fileManagement.saveList(login.getFiles());
						System.out.println(login.getFilesWeAlreadyHave());
						downloadAdapter.setFilesDownloadPanel(new JSonHandler()
								.getOldList());
					} else if (obj instanceof SharerIPsList) {
						needIps = false;
						SharerIPsList ips = (SharerIPsList) obj;
						for (SharersIPs s : ips.getListOfFilesToDownload()) {
							long id = s.getFileId();
							if (s.getAddresses().size() > 0) {
								for (String ip : s.getAddresses()) {
									if (ip == null)
										continue;
									System.out.println(ip);
									ip = ip.replace("/", "");
									new Thread(new P2PSocket(fileManagement,
											InetAddress.getByName(ip), id))
											.start();

								}
							}

						}
					}
					if (socketFails != 0)
						socketFails = 0;
				} catch (IOException | ClassNotFoundException e) {
					socketFails++;
					if (socketFails >= 10)
						keepConnection = false; // CONNECTION IS LOST (END OF
												// THREAD)
					System.out.println(socketFails + "outtttttttt22222");
					e.printStackTrace();
				}
				try { // we wait 1 second in order before sending more packets
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			System.out.println("____________");
		}
		try {// CLOSE
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("END OF THREAD (SocketAmbClient)");
	}

	public void updateList() {

	}

	public List<byte[]> getIPs(int idFile) {

		return null;
	}

	public String search() {
		return null;
	}

}
