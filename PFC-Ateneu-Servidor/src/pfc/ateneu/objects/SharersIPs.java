package pfc.ateneu.objects;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
/**
 * List of IPs of people who are connected and have the file we want to download
 * 
 * Control de les IP's disponibles com a fonts per descÓrregar el fitxer
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class SharersIPs implements Serializable{
	long fileId;
	List<String> addresses;
	public SharersIPs(long fileId) {
		this.fileId = fileId;
		addresses = new ArrayList<String>();
	}
	public List<String> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<String> addresses) {
		this.addresses = addresses;
	}
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	
}
