package pfc.ateneu.objects;

import java.io.Serializable;
/**
 * Piece used to send information about a file from the client to the server and viceversa. It is used to validate a new file in the server
 * and update the list
 * 
 * Pe�a principal del enviament d'informacio entre clients i el servidor, es fa servir per validar un nou fitxer al servidor
 * i tambe per actualitzar les llistes i enmagatzemar-les
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class SharedFile implements Serializable{
	String name;
	String md5;
	int  numberOfSharers;
	long size, id;
	boolean isNew;
	
	public SharedFile(String name, String md5, long id, int numberOfSharers, long size) {
		this.name = name;
		this.md5 = md5;
		this.id = id;
		this.numberOfSharers = numberOfSharers;
		this.size = size;
	}
	
	public SharedFile(String name, String md5, int numberOfSharers, long size, boolean isNew) {
		this.name = name;
		this.md5 = md5;
		this.numberOfSharers = numberOfSharers;
		this.size = size;
		this.isNew = isNew;
	}

	public int getNumberOfSharers() {
		return numberOfSharers;
	}



	public String getName() {
		return name;
	}
	public void setName(String nom) {
		this.name = nom;
	}
	public String getMd5() {
		return md5;
	}
	public void setMd5(String md5) {
		this.md5 = md5;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	@Override
	public String toString() {
		return "SharedFile [name=" + name + ", md5=" + md5 + ", id=" + id
				+ ", numberOfSharers=" + numberOfSharers + ", size=" + size
				+ ", isNew=" + isNew + "]";
	}
	
	
}
