package pfc.ateneu.main_i_sockets;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pfc.ateneu.objects.Login;
import pfc.ateneu.objects.SearchSharedFiles;
import pfc.ateneu.objects.SharedFile;
import pfc.ateneu.objects.SharerIPsList;
import pfc.ateneu.objects.SharersIPs;

/**
 * Manages the comunications between the server and the client
 * 
 * Control de les comunicacions entre el servidor i els usuaris
 * 
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */

public class ServerToPeerSocket implements Runnable {
	String serverIP = "192.168.1.201";
	ObjectInputStream in = null;
	ObjectOutputStream out = null;
	Socket socket = null;
	UserControlCon users;
	String ip = null;
	String mac = null;
	boolean keepConnection = true;
	int socketFails = 0; // if socket has failed 10 times without stop we close
							// this thread
	static Connection con;
	static String dbUser = "ausias";
	static String dbUserPass = "ausias";
	static String dbName = "pfc_ateneu";
	static Statement query;
	static ResultSet resultSet, auxiliarResultSet;

	public ServerToPeerSocket(Socket clientConnectat, UserControlCon users) {
		this.socket = clientConnectat;
		this.users = users;

	}

	@Override
	public void run() {
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			connectToDB();
			while (keepConnection) {
				Object obj = null;
				if (ip == null) {
					ip = socket.getInetAddress() + "";
					System.out.println("socket 1" + socket.getInetAddress());
				}
				try {
					System.out.println("Esperem");
					socket.setSoTimeout(60000);

					obj = (Object) in.readObject();

					System.out.println("Rebent del client: \n\t"
							+ obj.toString());
					query = con.createStatement();

					if (obj instanceof Login) {
						boolean newUser = false;
						Login login = (Login) obj;
						mac = login.getMac();
						users.setUser(mac, ip);

						resultSet = query
								.executeQuery("select count(mac) from users where mac = '"
										+ mac + "';");
						resultSet.next();
						// User doesn't exist
						if (resultSet.getInt(1) == 0) {
							query.executeUpdate("insert into users values('"
									+ mac + "',true, curdate());");
						}
						// User exists, update last connection
						else {
							query.executeUpdate("update users set connected = true, last_connection = curdate() where mac = '"
									+ mac + "';");

						}

						List<SharedFile> filesToAddOrDelete = login.getFiles();
						if (filesToAddOrDelete != null) {
							for (SharedFile file : filesToAddOrDelete) {
								System.out.println("fitxer de la llista "
										+ file.getName());
								if (file.isNew()) // this file is new (at least
													// for this client)
								{
									resultSet = query
											.executeQuery("select count(id) from files where md5='"
													+ file.getMd5() + "'");
									resultSet.next();
									int id;
									if (resultSet.getInt(1) == 0) {
										System.out.println("no el tenim "
												+ file.getId());
										query.executeUpdate("insert into files values(null,'"
												+ file.getMd5()
												+ "','"
												+ file.getSize()
												+ "','"
												+ file.getName() + "');");
										resultSet = query
												.executeQuery("select max(id) from files;");
										resultSet.next();
										id = resultSet.getInt(1);
									} else {
										System.out.println("el tenim "
												+ file.getId());
										resultSet = query
												.executeQuery("select id from files where md5='"
														+ file.getMd5() + "';");
										resultSet.next();
										id = resultSet.getInt(1);
									}
									System.out.println("primery keyy " + mac
											+ " | " + id);
									query.executeUpdate("insert into users_files values('"
											+ mac + "'," + id + ");");
									file.setId(id);

								} else// the user has removed this file, so now
										// he/she hasn't this file
								{
									System.out.println("eliminem "
											+ file.getId());
									query.executeUpdate("delete from users_files where user_id='"
											+ mac
											+ "' and file_id="
											+ file.getId() + ";");
									resultSet = query
											.executeQuery("select count(file_id) from users_files where file_id="
													+ file.getId());
									resultSet.next();
									if (resultSet.getInt(1) == 0)
										query.executeUpdate("delete from files where id="
												+ file.getId() + ";");
								}
							}
						}
						List<SharedFile> filesWithInfo = new ArrayList<SharedFile>();
						for (SharedFile shared : login.getFilesWeAlreadyHave()) {
							System.out.println(shared.toString());
							resultSet = query
									.executeQuery("select md5, size, name from files where id="
											+ shared.getId() + ";");
							resultSet.next();
							String md5 = resultSet.getString(1);
							long size = resultSet.getLong(2);
							String name = resultSet.getString(3);
							resultSet = query
									.executeQuery("select count(file_id) from users_files where file_id="
											+ shared.getId() + ";");
							resultSet.next();
							int numberOfSharers = resultSet.getInt(1);
							filesWithInfo.add(new SharedFile(name, md5, shared
									.getId(), numberOfSharers, size));
						}
						login.setFilesWeAlreadyHave(filesWithInfo);
						if (!newUser)
							out.writeObject(login);
						else
							out.writeObject("new_user");

					} else if (obj instanceof SharerIPsList) {
						SharerIPsList sharers = (SharerIPsList) obj;
						SharerIPsList IPs = new SharerIPsList();
						for (SharersIPs shaIP : sharers
								.getListOfFilesToDownload()) {
							System.out.println(shaIP.toString());
							resultSet = query
									.executeQuery("select user_id from users_files where file_id = "
											+ shaIP.getFileId() + " limit 30;");
							List<String> addresses = new ArrayList<String>();
							while (resultSet.next()) {
								System.out.println("SIIIIIIIIIII 0");
								String mac = resultSet.getString(1);
								System.out.println("mac " + mac);
								System.out
										.println("ip " + users.getUserIP(mac));
								System.out.println(users.toString());
								addresses.add(users.getUserIP(mac));
								System.out.println("-------------");
							}
							System.out.println("SIIIIIIIIIII ");
							shaIP.setAddresses(addresses);
							IPs.setFileInList(shaIP);
							System.out.println("SIIIIIIIIIII 2");
						}
						System.out.println("ENVIEMMM ");
						out.writeObject(IPs);
					} else if (obj instanceof SearchSharedFiles) {

						SearchSharedFiles searchFiles = (SearchSharedFiles) obj;
						String nameToMatch = searchFiles.getNameToMatch();
						List<SharedFile> sharedFilesList = new ArrayList<SharedFile>();

						resultSet = query
								.executeQuery("select * from files where name like '%"
										+ nameToMatch + "%' limit 300;");
						while (resultSet.next()) {
							int id = resultSet.getInt(1);
							auxiliarResultSet = query
									.executeQuery("select count(file_id) from users_files where file_id="
											+ id
											+ " and user_id='"
											+ mac
											+ "';");
							auxiliarResultSet.next();
							if (auxiliarResultSet.getInt(1) != 0) // USER HAS
																	// THIS FILE
																	// YET!
								continue;
							String md5 = resultSet.getString(2);
							String size = resultSet.getString(3);
							String fileName = resultSet.getString(4);
							auxiliarResultSet = query
									.executeQuery("select count(file_id) from users_files where file_id="
											+ id + ";");
							auxiliarResultSet.next();
							int numberOfSharers = auxiliarResultSet.getInt(1);
							System.out.println("size " + size);
							long longSize = Long.parseLong(size);
							System.out.println("long size " + longSize);
							sharedFilesList.add(new SharedFile(fileName, md5,
									id, numberOfSharers, longSize));
						}
						// fem SQL aquí select * from files where nom like
						// '%nom%' limit 300;
						// for(...)
						// sharedFilesList.add(new SharedFile(nomFitxer, md5,
						// id, size));
						searchFiles.setFiles(sharedFilesList);
						out.writeObject(searchFiles);
					} else if (obj instanceof String) {
						System.out.println((String) obj);
						out.writeObject("ok");
					}

					if (!users.isIpConnected(ip)) {
						users.setUser(mac, ip);
						// query = con.createStatement();
						query.executeUpdate("update users set connected = true where mac = '"
								+ mac + "';");
					}
					if (socketFails != 0)
						socketFails = 0;

					// disconnectFromDB();
				} catch (SocketTimeoutException e) {
					if (obj == null) {
						keepConnection = false;
						System.out.println("ip: " + socket.getInetAddress());
						connectToDB();
						if (mac != null && users.isIpConnected(ip)) {
							users.removeUser(mac);
							try {
								query.executeUpdate("update users set connected = false where mac = '"
										+ mac + "';");
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
						// disconnectFromDB();
					}
				} catch (IOException | ClassNotFoundException e) {
					socketFails++;
					if (socketFails >= 10)
						keepConnection = false; // CONNECTION IS LOST (END OF
												// THREAD)
					System.out.println("Socket with client failed "
							+ socketFails + "times");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (IOException e) {
			System.out
					.println("Connection with client has failed for some reason!");
			e.printStackTrace();
		}
		try {// CLOSE
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			System.out
					.println("Error closing socket and input/output streams!");
		}
		System.out.println("END OF THREAD (SocketAmbClient)");
	}
	/**
	 * Connection with DB in order to start the DB operations
	 * Conexio amb la base de dades per poguer comen�ar a realitzar les operacions
	 */
	public void connectToDB() {
		try {

			Class.forName("org.mariadb.jdbc.Driver");
			String url = "jdbc:mariadb://"+serverIP+":3306/" + dbName;
			con = DriverManager.getConnection(url, dbUser, dbUserPass);

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Disconnects from the DB
	 * Desconexio final de la base de dades
	 */
	public void disconnectFromDB() {
		try {
			query.close();
			con.close();

		} catch (SQLException e) {
			disconnectFromDB();
		}

	}

}
