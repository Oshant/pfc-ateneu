package pfc.ateneu.main_i_sockets;

import java.util.Hashtable;

/**
 * Management of the connection of users
 * 
 * Control de la conexi� dels usuaris
 * 
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class UserControlCon {
	private Hashtable<String, String> macToIp;
	
	/**
	 * Maps the MACs and IPs to allow the future connection among clients
	 * 
	 * Realitza un mapeig de les macs (ID a la base de dades) amb la IP per permetre la posterior conexio entre clients
	 */
	public UserControlCon() {
		macToIp = new Hashtable<String, String>();
	}
	/**
	 * Adds a new user
	 * @param mac user's MAC 
	 * @param IP Current user IP
	 * 
	 * Afegeix un nou usuari
	 * 
	 * @param mac MAC de l'usuari
	 * @param IP IP actual de l'usuari (ser� esborrada un cop desconectat)
	 */
	public void setUser(String mac, String IP) {
		System.out.println(mac);
		if (!macToIp.contains(mac)) {

			System.out.println(macToIp + " | " + macToIp.contains(mac));
			macToIp.put(mac, IP);

		}
	}
	/**
	 * Through the MAC addrress returns the related user from the DB
	 * @param mac
	 * @return returns NULL if the user doesn't exist and the IP if it actually exists
	 * 
	 * Mitjan�ant la MAC retorna el usuari corresponent de la DB
	 * @param mac
	 * @return retorna NULL si el usuari no existeix i la IP en cas de que existeixi
	 */
	public String getUserIP(String mac) {
		if (macToIp.containsKey(mac))
			return macToIp.get(mac);
		return null;
	}
	/**
	 * Removes the user from the HashMap
	 * 
	 * Elimina el usuari de la HashMap
	 * @param mac
	 */
	public void removeUser(String mac) {
		if (macToIp.contains(mac))
			macToIp.remove(mac);
	}
	/**
	 * Updates the information of connected/disconnected
	 * 
	 * Acualitza la informaci� de conectat/desconectat
	 * @param ip
	 * @return
	 */
	public boolean isIpConnected(String ip) {
		return macToIp.contains(ip);
	}

	@Override
	public String toString() {
		return "UserControlCon [macToIp=" + macToIp + "]";
	}

}
