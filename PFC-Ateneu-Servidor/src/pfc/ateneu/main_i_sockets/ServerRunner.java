package pfc.ateneu.main_i_sockets;
import java.io.IOException;
import java.net.ServerSocket;

/**
 * We get the input connections
 * 
 * Obtenim les connexions entrants
 * @author Roger Bosch
 * @author Ramon Padilla
 *
 */
public class ServerRunner implements Runnable{
	java.net.ServerSocket server = null;
    UserControlCon users;
	public ServerRunner() {
		try {
			server = new java.net.ServerSocket(Main.port);
		} catch (IOException e) {
			System.out.println("Could not open server socket!");
		}
		users = new UserControlCon();
	}
	
	@Override
	public void run() {
		
		try{
			
			while(true) 
			{
				System.out.println("Esperant el client  ");
				Thread thread = new Thread(new ServerToPeerSocket(server.accept(), users));
				thread.start(); //WE OPEN A NEW SOCKET FOR THE CONNECTED CLIENT    
			}       
		} catch (IOException e) {
			System.out.println("Client accept failed!");
		}
		
	}
}
